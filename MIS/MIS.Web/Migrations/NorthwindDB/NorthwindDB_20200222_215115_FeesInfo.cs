﻿using FluentMigrator;
using MIS.Northwind.Entities;
using Serenity.Data;
using System;

namespace MIS.Migrations.NorthwindDB
{

    [Migration(20200223_215115)]
    public class NorthwindDB_20200223_215115_FeesInfo : Migration
    {
        public override void Up()
        {
            Create.Table("FeesInfo")
                .WithColumn("Id").AsInt32().Identity()
                .WithColumn("StudentId").AsInt32().NotNullable().ForeignKey("StudentInfo", "Id")
                .WithColumn("PaymentNo").AsString().NotNullable()
                .WithColumn("StudentName").AsString().NotNullable()
                .WithColumn("AcadamicYearId").AsInt32().Nullable().ForeignKey("AcadamicYear","Id")
                .WithColumn("ClassId").AsInt32().Nullable().ForeignKey("StudentClass","Id")
                .WithColumn("ShiftId").AsInt32().Nullable().ForeignKey("Shift","Id")
                .WithColumn("CalculationMonth").AsDecimal().Nullable()
                .WithColumn("FeesRate").AsDecimal().Nullable()
                .WithColumn("FeesTotal").AsDecimal().Nullable()
                .WithColumn("Remarks").AsString().Nullable()
                ;

        }

        public override void Down() { }
    }


}