﻿using FluentMigrator;
using MIS.Northwind.Entities;
using Serenity.Data;
using System;

namespace MIS.Migrations.NorthwindDB
{

    [Migration(20200216_155115)]
    public class NorthwindDB_20200216_155115_StudentInfo : Migration
    {
        public override void Up()
        {
            Create.Table("StudentInfo")
                .WithColumn("Id").AsInt32().Identity()
                .WithColumn("RegistrationNo").AsString().NotNullable()
                .WithColumn("StudentName").AsString().NotNullable()
                .WithColumn("FatherName").AsString().NotNullable()
                .WithColumn("MotherName").AsString().NotNullable()
                .WithColumn("DateOfBirth").AsDateTime().NotNullable()
                .WithColumn("AcadamicYearId").AsInt32().NotNullable().ForeignKey("AcadamicYear","Id")
                .WithColumn("ClassId").AsInt32().NotNullable().ForeignKey("StudentClass","Id")
                .WithColumn("ShiftId").AsInt32().NotNullable().ForeignKey("Shift","Id")
                .WithColumn("Remarks").AsString().Nullable()
                ;

        }

        public override void Down() { }
    }


    [Migration(20200216_195115)]
    public class NorthwindDB_20200216_195115_StudentInfo : Migration
    {
        public override void Up()
        {
            Alter.Table("StudentInfo")
                .AddColumn("Gender").AsInt32().Nullable()
                ;

        }

        public override void Down() { }
    }

}