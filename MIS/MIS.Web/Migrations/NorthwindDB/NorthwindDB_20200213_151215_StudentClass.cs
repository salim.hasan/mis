﻿using FluentMigrator;
using MIS.Northwind.Entities;
using Serenity.Data;
using System;

namespace MIS.Migrations.NorthwindDB
{

    [Migration(20200213_151215)]
    public class NorthwindDB_20200213_151215_StudentClass : Migration
    {
        public override void Up()
        {
            Create.Table("StudentClass")
                .WithColumn("Id").AsInt32().Identity()
                //.WithColumn("BoatId").AsInt64().NotNullable().ForeignKey("Boat", "Id")
                .WithColumn("ClassName").AsString().NotNullable()
                .WithColumn("Description").AsString().Nullable()
                ;

        }

        public override void Down() { }
    }

    [Migration(20200213_151315)]
    public class NorthwindDB_20200213_151315_StudentClass : Migration
    {
        public override void Up()
        {
            Alter.Table("StudentClass")
                .AddColumn("ClassNumber").AsString().Nullable()
                ;



        }

        public override void Down() { }
    }
}