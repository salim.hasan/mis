﻿using FluentMigrator;
using MIS.Northwind.Entities;
using Serenity.Data;
using System;

namespace MIS.Migrations.NorthwindDB
{

    [Migration(20200213_151516)]
    public class NorthwindDB_20200213_151516_Shift : Migration
    {
        public override void Up()
        {
            Create.Table("Shift")
                .WithColumn("Id").AsInt32().Identity()
                //.WithColumn("BoatId").AsInt64().NotNullable().ForeignKey("Boat", "Id")
                .WithColumn("ClassId").AsInt32().NotNullable().ForeignKey("StudentClass", "Id")
                .WithColumn("ShiftName").AsString().NotNullable()
                .WithColumn("Description").AsString().Nullable()
                ;

        }

        public override void Down() { }
    }
}