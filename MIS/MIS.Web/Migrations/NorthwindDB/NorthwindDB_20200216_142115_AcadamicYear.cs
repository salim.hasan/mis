﻿using FluentMigrator;
using MIS.Northwind.Entities;
using Serenity.Data;
using System;

namespace MIS.Migrations.NorthwindDB
{

    [Migration(20200216_142115)]
    public class NorthwindDB_20200216_142115_AcadamicYear : Migration
    {
        public override void Up()
        {
            Create.Table("AcadamicYear")
                .WithColumn("Id").AsInt32().Identity()
                //.WithColumn("BoatId").AsInt64().NotNullable().ForeignKey("Boat", "Id")
                .WithColumn("AcadamicYearName").AsString().NotNullable()
                .WithColumn("StartDate").AsDateTime().NotNullable()
                .WithColumn("EndDate").AsDateTime().NotNullable()
                .WithColumn("Description").AsString().Nullable()
                ;

        }

        public override void Down() { }
    }

}