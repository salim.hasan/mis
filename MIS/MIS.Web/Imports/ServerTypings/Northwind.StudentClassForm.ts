﻿namespace MIS.Northwind {
    export interface StudentClassForm {
        ClassName: Serenity.StringEditor;
        ClassNumber: Serenity.StringEditor;
        Description: Serenity.StringEditor;
    }

    export class StudentClassForm extends Serenity.PrefixedContext {
        static formKey = 'Northwind.StudentClass';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!StudentClassForm.init)  {
                StudentClassForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;

                Q.initFormType(StudentClassForm, [
                    'ClassName', w0,
                    'ClassNumber', w0,
                    'Description', w0
                ]);
            }
        }
    }
}

