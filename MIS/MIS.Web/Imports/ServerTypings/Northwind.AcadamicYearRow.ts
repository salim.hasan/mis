﻿namespace MIS.Northwind {
    export interface AcadamicYearRow {
        Id?: number;
        AcadamicYearName?: string;
        StartDate?: string;
        EndDate?: string;
        Description?: string;
    }

    export namespace AcadamicYearRow {
        export const idProperty = 'Id';
        export const nameProperty = 'AcadamicYearName';
        export const localTextPrefix = 'Northwind.AcadamicYear';
        export const lookupKey = 'Northwind.AcadamicYear';

        export function getLookup(): Q.Lookup<AcadamicYearRow> {
            return Q.getLookup<AcadamicYearRow>('Northwind.AcadamicYear');
        }
        export const deletePermission = 'Northwind:General';
        export const insertPermission = 'Northwind:General';
        export const readPermission = 'Northwind:General';
        export const updatePermission = 'Northwind:General';

        export declare const enum Fields {
            Id = "Id",
            AcadamicYearName = "AcadamicYearName",
            StartDate = "StartDate",
            EndDate = "EndDate",
            Description = "Description"
        }
    }
}

