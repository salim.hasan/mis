﻿namespace MIS.Northwind {
    export interface FeesInfoForm {
        StudentId: Serenity.LookupEditor;
        PaymentNo: Serenity.StringEditor;
        StudentName: Serenity.StringEditor;
        AcadamicYearId: Serenity.LookupEditor;
        ClassName: Serenity.StringEditor;
        ShiftName: Serenity.StringEditor;
        CalculationMonth: Serenity.DecimalEditor;
        FeesRate: Serenity.DecimalEditor;
        FeesTotal: Serenity.DecimalEditor;
        Remarks: Serenity.TextAreaEditor;
    }

    export class FeesInfoForm extends Serenity.PrefixedContext {
        static formKey = 'Northwind.FeesInfo';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!FeesInfoForm.init)  {
                FeesInfoForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;
                var w1 = s.StringEditor;
                var w2 = s.DecimalEditor;
                var w3 = s.TextAreaEditor;

                Q.initFormType(FeesInfoForm, [
                    'StudentId', w0,
                    'PaymentNo', w1,
                    'StudentName', w1,
                    'AcadamicYearId', w0,
                    'ClassName', w1,
                    'ShiftName', w1,
                    'CalculationMonth', w2,
                    'FeesRate', w2,
                    'FeesTotal', w2,
                    'Remarks', w3
                ]);
            }
        }
    }
}

