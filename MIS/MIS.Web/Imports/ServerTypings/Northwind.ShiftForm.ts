﻿namespace MIS.Northwind {
    export interface ShiftForm {
        ClassId: Serenity.LookupEditor;
        ShiftName: Serenity.StringEditor;
        Description: Serenity.TextAreaEditor;
    }

    export class ShiftForm extends Serenity.PrefixedContext {
        static formKey = 'Northwind.Shift';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!ShiftForm.init)  {
                ShiftForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;
                var w1 = s.StringEditor;
                var w2 = s.TextAreaEditor;

                Q.initFormType(ShiftForm, [
                    'ClassId', w0,
                    'ShiftName', w1,
                    'Description', w2
                ]);
            }
        }
    }
}

