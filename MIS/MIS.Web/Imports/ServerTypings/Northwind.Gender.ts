﻿namespace MIS.Northwind {
    export enum Gender {
        Male = 1,
        Female = 2
    }
    Serenity.Decorators.registerEnumType(Gender, 'MIS.Northwind.Gender', 'MIS.Northwind.Entities.Gender');
}

