﻿namespace MIS.Northwind {
    export interface StudentClassRow {
        Id?: number;
        ClassName?: string;
        ClassNumber?: string;
        Description?: string;
    }

    export namespace StudentClassRow {
        export const idProperty = 'Id';
        export const nameProperty = 'ClassName';
        export const localTextPrefix = 'Northwind.StudentClass';
        export const lookupKey = 'Northwind.StudentClass';

        export function getLookup(): Q.Lookup<StudentClassRow> {
            return Q.getLookup<StudentClassRow>('Northwind.StudentClass');
        }
        export const deletePermission = 'Northwind:General';
        export const insertPermission = 'Northwind:General';
        export const readPermission = 'Northwind:General';
        export const updatePermission = 'Northwind:General';

        export declare const enum Fields {
            Id = "Id",
            ClassName = "ClassName",
            ClassNumber = "ClassNumber",
            Description = "Description"
        }
    }
}

