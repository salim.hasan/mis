﻿namespace MIS.Northwind {
    export namespace FeesInfoService {
        export const baseUrl = 'Northwind/FeesInfo';

        export declare function Create(request: Serenity.SaveRequest<FeesInfoRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Update(request: Serenity.SaveRequest<FeesInfoRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<FeesInfoRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<FeesInfoRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function GetNextPaymentNo(request: GetNextNumberRequest, onSuccess?: (response: GetNextNumberResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export declare const enum Methods {
            Create = "Northwind/FeesInfo/Create",
            Update = "Northwind/FeesInfo/Update",
            Delete = "Northwind/FeesInfo/Delete",
            Retrieve = "Northwind/FeesInfo/Retrieve",
            List = "Northwind/FeesInfo/List",
            GetNextPaymentNo = "Northwind/FeesInfo/GetNextPaymentNo"
        }

        [
            'Create', 
            'Update', 
            'Delete', 
            'Retrieve', 
            'List', 
            'GetNextPaymentNo'
        ].forEach(x => {
            (<any>FeesInfoService)[x] = function (r, s, o) {
                return Q.serviceRequest(baseUrl + '/' + x, r, s, o);
            };
        });
    }
}

