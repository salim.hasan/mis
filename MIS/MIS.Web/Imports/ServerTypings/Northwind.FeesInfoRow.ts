﻿namespace MIS.Northwind {
    export interface FeesInfoRow {
        Id?: number;
        AcadamicYearId?: number;
        AcadamicYear?: string;
        StudentId?: number;
        ClassId?: number;
        ClassName?: string;
        ShiftId?: number;
        ShiftName?: string;
        StudentName?: string;
        PaymentNo?: string;
        CalculationMonth?: number;
        FeesRate?: number;
        FeesTotal?: number;
        Registration?: string;
        Remarks?: string;
    }

    export namespace FeesInfoRow {
        export const idProperty = 'Id';
        export const nameProperty = 'StudentName';
        export const localTextPrefix = 'Northwind.FeesInfo';
        export const lookupKey = 'Northwind.FeesInfo';

        export function getLookup(): Q.Lookup<FeesInfoRow> {
            return Q.getLookup<FeesInfoRow>('Northwind.FeesInfo');
        }
        export const deletePermission = 'Northwind:General';
        export const insertPermission = 'Northwind:General';
        export const readPermission = 'Northwind:General';
        export const updatePermission = 'Northwind:General';

        export declare const enum Fields {
            Id = "Id",
            AcadamicYearId = "AcadamicYearId",
            AcadamicYear = "AcadamicYear",
            StudentId = "StudentId",
            ClassId = "ClassId",
            ClassName = "ClassName",
            ShiftId = "ShiftId",
            ShiftName = "ShiftName",
            StudentName = "StudentName",
            PaymentNo = "PaymentNo",
            CalculationMonth = "CalculationMonth",
            FeesRate = "FeesRate",
            FeesTotal = "FeesTotal",
            Registration = "Registration",
            Remarks = "Remarks"
        }
    }
}

