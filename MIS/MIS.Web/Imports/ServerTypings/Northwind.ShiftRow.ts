﻿namespace MIS.Northwind {
    export interface ShiftRow {
        Id?: number;
        ClassId?: number;
        ShiftName?: string;
        Description?: string;
        ClassName?: string;
    }

    export namespace ShiftRow {
        export const idProperty = 'Id';
        export const nameProperty = 'ShiftName';
        export const localTextPrefix = 'Northwind.Shift';
        export const lookupKey = 'Northwind.Shift';

        export function getLookup(): Q.Lookup<ShiftRow> {
            return Q.getLookup<ShiftRow>('Northwind.Shift');
        }
        export const deletePermission = 'Northwind:General';
        export const insertPermission = 'Northwind:General';
        export const readPermission = 'Northwind:General';
        export const updatePermission = 'Northwind:General';

        export declare const enum Fields {
            Id = "Id",
            ClassId = "ClassId",
            ShiftName = "ShiftName",
            Description = "Description",
            ClassName = "ClassName"
        }
    }
}

