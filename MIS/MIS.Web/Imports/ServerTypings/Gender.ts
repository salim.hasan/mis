﻿namespace MIS {
    export enum Gender {
        Male = 1,
        Female = 2
    }
    Serenity.Decorators.registerEnumType(Gender, 'MIS.Gender', 'Gender');
}

