﻿namespace MIS.Northwind {
    export interface StudentInfoRow {
        Id?: number;
        AcadamicYearId?: number;
        RegistrationNo?: string;
        ClassId?: number;
        ClassName?: string;
        ShiftId?: number;
        ShiftName?: string;
        StudentName?: string;
        FatherName?: string;
        MotherName?: string;
        DateOfBirth?: string;
        Gender?: Gender;
        Remarks?: string;
    }

    export namespace StudentInfoRow {
        export const idProperty = 'Id';
        export const nameProperty = 'RegistrationNo';
        export const localTextPrefix = 'Northwind.StudentInfo';
        export const lookupKey = 'Northwind.StudentInfo';

        export function getLookup(): Q.Lookup<StudentInfoRow> {
            return Q.getLookup<StudentInfoRow>('Northwind.StudentInfo');
        }
        export const deletePermission = 'Northwind:General';
        export const insertPermission = 'Northwind:General';
        export const readPermission = 'Northwind:General';
        export const updatePermission = 'Northwind:General';

        export declare const enum Fields {
            Id = "Id",
            AcadamicYearId = "AcadamicYearId",
            RegistrationNo = "RegistrationNo",
            ClassId = "ClassId",
            ClassName = "ClassName",
            ShiftId = "ShiftId",
            ShiftName = "ShiftName",
            StudentName = "StudentName",
            FatherName = "FatherName",
            MotherName = "MotherName",
            DateOfBirth = "DateOfBirth",
            Gender = "Gender",
            Remarks = "Remarks"
        }
    }
}

