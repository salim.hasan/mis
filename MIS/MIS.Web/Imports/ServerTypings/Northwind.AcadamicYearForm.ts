﻿namespace MIS.Northwind {
    export interface AcadamicYearForm {
        AcadamicYearName: Serenity.StringEditor;
        StartDate: Serenity.DateEditor;
        EndDate: Serenity.DateEditor;
        Description: Serenity.TextAreaEditor;
    }

    export class AcadamicYearForm extends Serenity.PrefixedContext {
        static formKey = 'Northwind.AcadamicYear';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!AcadamicYearForm.init)  {
                AcadamicYearForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;
                var w1 = s.DateEditor;
                var w2 = s.TextAreaEditor;

                Q.initFormType(AcadamicYearForm, [
                    'AcadamicYearName', w0,
                    'StartDate', w1,
                    'EndDate', w1,
                    'Description', w2
                ]);
            }
        }
    }
}

