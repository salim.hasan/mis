﻿namespace MIS.Northwind {
    export namespace StudentInfoService {
        export const baseUrl = 'Northwind/StudentInfo';

        export declare function Create(request: Serenity.SaveRequest<StudentInfoRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Update(request: Serenity.SaveRequest<StudentInfoRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<StudentInfoRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<StudentInfoRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function GetNextRegistrationNo(request: GetNextNumberRequest, onSuccess?: (response: GetNextNumberResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export declare const enum Methods {
            Create = "Northwind/StudentInfo/Create",
            Update = "Northwind/StudentInfo/Update",
            Delete = "Northwind/StudentInfo/Delete",
            Retrieve = "Northwind/StudentInfo/Retrieve",
            List = "Northwind/StudentInfo/List",
            GetNextRegistrationNo = "Northwind/StudentInfo/GetNextRegistrationNo"
        }

        [
            'Create', 
            'Update', 
            'Delete', 
            'Retrieve', 
            'List', 
            'GetNextRegistrationNo'
        ].forEach(x => {
            (<any>StudentInfoService)[x] = function (r, s, o) {
                return Q.serviceRequest(baseUrl + '/' + x, r, s, o);
            };
        });
    }
}

