﻿namespace MIS.Northwind {
    export interface StudentInfoForm {
        AcadamicYearId: Serenity.LookupEditor;
        RegistrationNo: Serenity.StringEditor;
        ClassId: Serenity.LookupEditor;
        ShiftId: Serenity.LookupEditor;
        StudentName: Serenity.StringEditor;
        FatherName: Serenity.StringEditor;
        MotherName: Serenity.StringEditor;
        DateOfBirth: Serenity.DateEditor;
        Gender: Serenity.EnumEditor;
        Remarks: Serenity.TextAreaEditor;
    }

    export class StudentInfoForm extends Serenity.PrefixedContext {
        static formKey = 'Northwind.StudentInfo';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!StudentInfoForm.init)  {
                StudentInfoForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;
                var w1 = s.StringEditor;
                var w2 = s.DateEditor;
                var w3 = s.EnumEditor;
                var w4 = s.TextAreaEditor;

                Q.initFormType(StudentInfoForm, [
                    'AcadamicYearId', w0,
                    'RegistrationNo', w1,
                    'ClassId', w0,
                    'ShiftId', w0,
                    'StudentName', w1,
                    'FatherName', w1,
                    'MotherName', w1,
                    'DateOfBirth', w2,
                    'Gender', w3,
                    'Remarks', w4
                ]);
            }
        }
    }
}

