﻿
namespace MIS.Northwind.Entities
{
    using MIS.Northwind.Repositories;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), Module("Northwind"), TableName("FeesInfo")]
    [DisplayName("FeesInfo"), InstanceName("FeesInfo")]
    [ReadPermission(PermissionKeys.General)]
    [ModifyPermission(PermissionKeys.General)]
    [LookupScript]
    public sealed class FeesInfoRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Payment No."), Size(15), NotNull, QuickSearch,ReadOnly(true)]
        public String PaymentNo
        {
            get { return Fields.PaymentNo[this]; }
            set { Fields.PaymentNo[this] = value; }
        }

        [DisplayName("Acadamic Year"),NotNull]
        [ForeignKey(typeof(AcadamicYearRow)), LeftJoin("jAcadamicYear")]
        [LookupEditor(typeof(AcadamicYearRow)), QuickFilter]
        public Int32? AcadamicYearId
        {
            get { return Fields.AcadamicYearId[this]; }
            set { Fields.AcadamicYearId[this] = value; }
        }

        #region Registration No.

        [DisplayName("Registration No."), Size(15), NotNull, QuickSearch]
        [ForeignKey("StudentInfo","Id"), LeftJoin("jStudentId")]
        [LookupEditor(typeof(StudentInfoRow)), QuickFilter]
        public Int32? StudentId
        {
            get { return Fields.StudentId[this]; }
            set { Fields.StudentId[this] = value; }
        }

        StudentInfoRow _Student;
        public StudentInfoRow Student
        {
            get
            {
                _Student = _Student ?? new StudentInfoRepository().Retrieve(SqlConnections.NewFor<StudentInfoRow>(), new Serenity.Services.RetrieveRequest { EntityId = StudentId }).Entity;

                return _Student;
            }
        }

        #endregion Registration No.s

        [DisplayName("Class Name"), NotNull,ReadOnly(true)]
        [ForeignKey(typeof(StudentClassRow)), LeftJoin("jStudentClass")]
        [LookupEditor(typeof(StudentClassRow)), QuickFilter]
        public Int32? ClassId
        {
            get { return Fields.ClassId[this]; }
            set { Fields.ClassId[this] = value; }
        }

        [DisplayName("Shift Name"), NotNull,ReadOnly(true)]
        [ForeignKey(typeof(ShiftRow)), LeftJoin("jStudentShift")]
        [LookupEditor(typeof(ShiftRow)), QuickFilter]
        public Int32? ShiftId
        {
            get { return Fields.ShiftId[this]; }
            set { Fields.ShiftId[this] = value; }
        }

        [DisplayName("Student Name"), Size(15), NotNull, QuickSearch,ReadOnly(true)]
        public String StudentName
        {
            get { return Fields.StudentName[this]; }
            set { Fields.StudentName[this] = value; }
        }

        //[DisplayName("Father'S Name"), Size(15), NotNull, QuickSearch]
        //public String FatherName
        //{
        //    get { return Fields.FatherName[this]; }
        //    set { Fields.FatherName[this] = value; }
        //}

        //[DisplayName("Mother'S Name"), Size(15), NotNull, QuickSearch]
        //public String MotherName
        //{
        //    get { return Fields.MotherName[this]; }
        //    set { Fields.MotherName[this] = value; }
        //}

        [DisplayName("Calculation Month"), Size(19), Scale(2), NotNull, DefaultValue(0)]
        public Decimal? CalculationMonth
        {
            get { return Fields.CalculationMonth[this]; }
            set { Fields.CalculationMonth[this] = value; }
        }

        [DisplayName("Fees Rate"), Size(19), Scale(2), NotNull, DefaultValue(0)]
        public Decimal? FeesRate
        {
            get { return Fields.FeesRate[this]; }
            set { Fields.FeesRate[this] = value; }
        }

        [DisplayName("Fees Total"), Size(19), Scale(2), ReadOnly(true), NotNull, DefaultValue(0)]
        public Decimal? FeesTotal
        {
            get { return Fields.FeesTotal[this]; }
            set { Fields.FeesTotal[this] = value; }
        }


        [DisplayName("Remarks"), QuickSearch]
        public String Remarks
        {
            get { return Fields.Remarks[this]; }
            set { Fields.Remarks[this] = value; }
        }


        #region ForeignFiels

        [DisplayName("Class Name"), Expression("jStudentClass.[ClassName]"),ReadOnly(true)]
        public String ClassName
        {
            get { return Fields.ClassName[this]; }
            set { Fields.ClassName[this] = value; }
        }

        [DisplayName("Shift Name"), Expression("jStudentShift.[ShiftName]")]
        public String ShiftName
        {
            get { return Fields.ShiftName[this]; }
            set { Fields.ShiftName[this] = value; }
        }

        [DisplayName("Registration"), Expression("jStudentId.[RegistrationNo]")]
        public String Registration
        {
            get { return Fields.Registration[this]; }
            set { Fields.Registration[this] = value; }
        }


        [DisplayName("Admission Year"), Expression("jAcadamicYear.[AcadamicYearName]")]
        public String AcadamicYear
        {
            get { return Fields.AcadamicYear[this]; }
            set { Fields.AcadamicYear[this] = value; }
        }

        #endregion ForeignFiels

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }


        StringField INameRow.NameField
        {
            get { return Fields.StudentName; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public FeesInfoRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public Int32Field AcadamicYearId;
            public StringField AcadamicYear;
            public Int32Field StudentId;
            public Int32Field ClassId;
            public StringField ClassName;
            public Int32Field ShiftId;
            public StringField ShiftName;
            public StringField StudentName;
            //public StringField FatherName;
            //public StringField MotherName;
            public StringField PaymentNo;
            public DecimalField CalculationMonth;
            public DecimalField FeesRate;
            public DecimalField FeesTotal;
            public StringField Registration;
            public StringField Remarks;
        }
    }
}