﻿
namespace MIS.Northwind.Forms
{
    using Serenity.ComponentModel;
    using System;

    [FormScript("Northwind.FeesInfo")]
    [BasedOnRow(typeof(Entities.FeesInfoRow), CheckNames = true)]
    public class FeesInfoForm
    {
        [HalfWidth(UntilNext = true)]
        public Int32 StudentId { get; set; }
        public String PaymentNo { get; set; }
        
        //[FullWidth]
        public String StudentName { get; set; }
        [HalfWidth(UntilNext = true)]
        public Int32 AcadamicYearId { get; set; }
        public String ClassName { get; set; }
        public String ShiftName { get; set; }


        [QuarterWidth(UntilNext =true)]
        public Decimal CalculationMonth { get; set; }
        public Decimal FeesRate { get; set; }
        public Decimal FeesTotal { get; set; }

        [FullWidth]
        [TextAreaEditor(Rows = 3)]
        public String Remarks { get; set; }
    }
}