﻿
namespace MIS.Northwind.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Northwind/FeesInfo"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.FeesInfoRow))]
    public class FeesInfoController : Controller
    {
        public ActionResult Index()
        {
            return View(MVC.Views.Northwind.FeesInfo.FeesInfoIndex);
        }
    }
}
