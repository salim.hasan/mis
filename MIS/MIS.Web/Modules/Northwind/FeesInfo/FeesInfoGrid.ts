﻿namespace MIS.Northwind{

    @Serenity.Decorators.registerClass()
    export class FeesInfoGrid extends Serenity.EntityGrid<FeesInfoRow, any> {
        protected getColumnsKey() { return "Northwind.FeesInfo"; }
        protected getDialogType() { return <any>FeesInfoDialog; }
        protected getIdProperty() { return FeesInfoRow.idProperty; }
        protected getLocalTextPrefix() { return FeesInfoRow.localTextPrefix; }
        protected getService() { return FeesInfoService.baseUrl; }

        protected getDisplayName() { return 'Student Tution Fees Information' }

        constructor(container: JQuery) {
            super(container);
        }
    }
}