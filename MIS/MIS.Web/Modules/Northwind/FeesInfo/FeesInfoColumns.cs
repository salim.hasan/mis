﻿
namespace MIS.Northwind.Forms
{
    using Serenity.ComponentModel;
    using System;
    using System.ComponentModel;

    [ColumnsScript("Northwind.FeesInfo")]
    [BasedOnRow(typeof(Entities.FeesInfoRow), CheckNames = true)]
    public class FeesInfoColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight,Hidden]
        public Int32 Id { get; set; }
        [EditLink]
        public String PaymentNo { get; set; }
        public String Registration { get; set; }
        public String StudentName { get; set; }
        public String ClassName { get; set; }
        public String ShiftName { get; set; }
        public String AcadamicYear { get; set; }
        public Int32 CalculationMonth { get; set; }
        public Int32 FeesRate { get; set; }
        public Int32 FeesTotal { get; set; }

    }
}