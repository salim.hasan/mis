﻿namespace MIS.Northwind {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    @Serenity.Decorators.maximizable()
    export class FeesInfoDialog extends Serenity.EntityDialog<FeesInfoRow, any> {
        protected getFormKey() { return FeesInfoForm.formKey; }
        protected getIdProperty() { return FeesInfoRow.idProperty; }
        protected getLocalTextPrefix() { return FeesInfoRow.localTextPrefix; }
        protected getNameProperty() { return FeesInfoRow.nameProperty; }
        protected getService() { return FeesInfoService.baseUrl; }

        protected form = new FeesInfoForm(this.idPrefix);


        constructor() {
            super();

            this.form.CalculationMonth.change(e => {
                this.FeesChange();
            });

            this.form.FeesRate.change(e => {
                this.FeesChange();
            });

            this.form.StudentId.changeSelect2(e => {
                this.LoadStudent();
            });
        }

        private FeesChange() {
            var rate = q.ToNumber(this.form.FeesRate.value);
            var mTon = q.ToNumber(this.form.CalculationMonth.value);

            var total = mTon * rate;
            this.form.FeesTotal.value = total;
            //this.form.RiverDuesVat.value = total * vatPer / 100.00;

            //this.CalculateTotalnVAT();
        }

        protected afterLoadEntity() {
            super.afterLoadEntity();

            if (this.isNew()) {
                this.getNextNumber();
            }
        }

        private getNextNumber() {

            var val = Q.trimToNull(this.form.PaymentNo.value);

            if (!val || val.length <= 1) {

                var prefix = (val || 'KISC').toUpperCase();

                FeesInfoService.GetNextPaymentNo({
                    Prefix: prefix,
                    Length: prefix.length + 6
                }, response => {
                    this.form.PaymentNo.value = response.Serial;

                    // this is to mark numerical part after prefix
                    //(this.form.AssessmentNo.element[0] as any).setSelectionRange(prefix.length, response.Serial.length);
                });
            }
        }

        LoadStudent() {
            StudentInfoService.Retrieve({
                EntityId: this.entity.StudentId || this.form.StudentId.value,
                ColumnSelection: Serenity.RetrieveColumnSelection.details
            }, Response => {
                let student = Response.Entity;
                this.form.StudentName.value = student.StudentName;
                this.form.AcadamicYearId.value = String(student.AcadamicYearId);
                this.form.ClassName.value = String(student.ClassName);
                this.form.ShiftName.value = String(student.ShiftName);
            });
        }

    }
}