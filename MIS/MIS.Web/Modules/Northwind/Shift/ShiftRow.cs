﻿
namespace MIS.Northwind.Entities
{
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), Module("Northwind"), TableName("Shift")]
    [DisplayName("Shift"), InstanceName("Shift")]
    [ReadPermission(PermissionKeys.General)]
    [ModifyPermission(PermissionKeys.General)]
    [LookupScript]
    public sealed class ShiftRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Class Name"),NotNull]
        [ForeignKey(typeof(StudentClassRow)), LeftJoin("jStudentClass")]
        [LookupEditor(typeof(StudentClassRow)), QuickFilter]
        public Int32? ClassId
        {
            get { return Fields.ClassId[this]; }
            set { Fields.ClassId[this] = value; }
        }
        

        [DisplayName("Shift Name"), Size(15), NotNull, QuickSearch]
        public String ShiftName
        {
            get { return Fields.ShiftName[this]; }
            set { Fields.ShiftName[this] = value; }
        }

        

        [DisplayName("Description"), QuickSearch]
        public String Description
        {
            get { return Fields.Description[this]; }
            set { Fields.Description[this] = value; }
        }


        [DisplayName("Class Name"), Expression("jStudentClass.[ClassName]")]
        public String ClassName
        {   get { return Fields.ClassName[this]; }
            set { Fields.ClassName[this] = value; }
        }
        



        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.ShiftName; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public ShiftRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public Int32Field ClassId;
            public StringField ShiftName;
            public StringField Description;
            public StringField ClassName;
        }
    }
}