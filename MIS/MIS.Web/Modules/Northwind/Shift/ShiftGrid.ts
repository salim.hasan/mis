﻿namespace MIS.Northwind {

    @Serenity.Decorators.registerClass()
    export class CategoryGrid extends Serenity.EntityGrid<ShiftRow, any> {
        protected getColumnsKey() { return "Northwind.Shift"; }
        protected getDialogType() { return <any>ShiftDialog; }
        protected getIdProperty() { return ShiftRow.idProperty; }
        protected getLocalTextPrefix() { return ShiftRow.localTextPrefix; }
        protected getService() { return ShiftService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}