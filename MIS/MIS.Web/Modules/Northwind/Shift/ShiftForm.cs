﻿
namespace MIS.Northwind.Forms
{
    using Serenity.ComponentModel;
    using System;

    [FormScript("Northwind.Shift")]
    [BasedOnRow(typeof(Entities.ShiftRow), CheckNames = true)]
    public class ShiftForm
    {
        [HalfWidth]
        public String ClassId { get; set; }
        [HalfWidth]
        public String ShiftName { get; set; }
        [TextAreaEditor(Rows = 3)]
        public String Description { get; set; }
    }
}