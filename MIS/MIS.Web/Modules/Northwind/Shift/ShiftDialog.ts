﻿namespace MIS.Northwind {

    @Serenity.Decorators.registerClass()
    export class ShiftDialog extends Serenity.EntityDialog<ShiftRow, any> {
        protected getFormKey() { return ShiftForm.formKey; }
        protected getIdProperty() { return ShiftRow.idProperty; }
        protected getLocalTextPrefix() { return ShiftRow.localTextPrefix; }
        protected getNameProperty() { return ShiftRow.nameProperty; }
        protected getService() { return ShiftService.baseUrl; }

        protected form = new ShiftForm(this.idPrefix);
    }
}