﻿
namespace MIS.Northwind.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Northwind/Shift"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.ShiftRow))]
    public class ShiftController : Controller
    {
        public ActionResult Index()
        {
            return View(MVC.Views.Northwind.Category.CategoryIndex);
        }
    }
}
