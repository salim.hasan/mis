﻿
namespace MIS.Northwind.Forms
{
    using Serenity.ComponentModel;
    using System;
    using System.ComponentModel;

    [ColumnsScript("Northwind.Shift")]
    [BasedOnRow(typeof(Entities.ShiftRow), CheckNames = true)]
    public class ShiftColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight,Hidden]
        public Int32 Id { get; set; }
        [EditLink]
        public String ShiftName { get; set; }
        public String ClassName { get; set; }
        [FullWidth]
        public String Description { get; set; }
    }
}