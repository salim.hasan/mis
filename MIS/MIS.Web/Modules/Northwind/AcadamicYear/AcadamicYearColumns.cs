﻿
namespace MIS.Northwind.Forms
{
    using Serenity.ComponentModel;
    using System;
    using System.ComponentModel;

    [ColumnsScript("Northwind.AcadamicYear")]
    [BasedOnRow(typeof(Entities.AcadamicYearRow), CheckNames = true)]
    public class AcadamicYearColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight,Hidden]
        public Int32 Id { get; set; }
        [EditLink]
        public String AcadamicYearName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        [FullWidth]
        public String Description { get; set; }
    }
}