﻿namespace MIS.Northwind {

    @Serenity.Decorators.registerClass()
    export class AcadamicYearDialog extends Serenity.EntityDialog<AcadamicYearRow, any> {
        protected getFormKey() { return AcadamicYearForm.formKey; }
        protected getIdProperty() { return AcadamicYearRow.idProperty; }
        protected getLocalTextPrefix() { return AcadamicYearRow.localTextPrefix; }
        protected getNameProperty() { return AcadamicYearRow.nameProperty; }
        protected getService() { return AcadamicYearService.baseUrl; }

        protected form = new AcadamicYearForm(this.idPrefix);
    }
}