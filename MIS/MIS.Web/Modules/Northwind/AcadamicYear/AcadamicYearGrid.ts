﻿namespace MIS.Northwind {

    @Serenity.Decorators.registerClass()
    export class AcadamicYearGrid extends Serenity.EntityGrid<AcadamicYearRow, any> {
        protected getColumnsKey() { return "Northwind.AcadamicYear"; }
        protected getDialogType() { return <any>AcadamicYearDialog; }
        protected getIdProperty() { return AcadamicYearRow.idProperty; }
        protected getLocalTextPrefix() { return AcadamicYearRow.localTextPrefix; }
        protected getService() { return AcadamicYearService.baseUrl; }

        protected getDisplayName() { return 'Academic Year Information'; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}