﻿
namespace MIS.Northwind.Forms
{
    using Serenity.ComponentModel;
    using System;

    [FormScript("Northwind.AcadamicYear")]
    [BasedOnRow(typeof(Entities.AcadamicYearRow), CheckNames = true)]
    public class AcadamicYearForm
    {
        
        public String AcadamicYearName { get; set; }
        [HalfWidth]
        public DateTime? StartDate { get; set; }
        [HalfWidth]
        public DateTime? EndDate { get; set; }
        [TextAreaEditor(Rows = 3)]
        public String Description { get; set; }
    }
}