﻿
namespace MIS.Northwind.Entities
{
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), Module("Northwind"), TableName("AcadamicYear")]
    [DisplayName("AcadamicYear"), InstanceName("AcadamicYear")]
    [ReadPermission(PermissionKeys.General)]
    [ModifyPermission(PermissionKeys.General)]
    [LookupScript]
    public sealed class AcadamicYearRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }


        [DisplayName("Acadamic Year"), Size(15), NotNull, QuickSearch]
        public String AcadamicYearName
        {
            get { return Fields.AcadamicYearName[this]; }
            set { Fields.AcadamicYearName[this] = value; }
        }

        [DisplayName("Start Date"), QuickSearch]
        public DateTime? StartDate
        {
            get { return Fields.StartDate[this]; }
            set { Fields.StartDate[this] = value; }
        }

        [DisplayName("End Date"), QuickSearch]
        public DateTime? EndDate
        {
            get { return Fields.EndDate[this]; }
            set { Fields.EndDate[this] = value; }
        }


        [DisplayName("Description"), QuickSearch]
        public String Description
        {
            get { return Fields.Description[this]; }
            set { Fields.Description[this] = value; }
        }




        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.AcadamicYearName; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AcadamicYearRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public StringField AcadamicYearName;
            public DateTimeField StartDate;
            public DateTimeField EndDate;
            public StringField Description;
        }
    }
}