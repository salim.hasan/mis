﻿
namespace MIS.Northwind.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Northwind/AcadamicYear"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.ShiftRow))]
    public class AcadamicYearController : Controller
    {
        public ActionResult Index()
        {
            return View(MVC.Views.Northwind.AcadamicYear.AcadamicYearIndex);
        }
    }
}
