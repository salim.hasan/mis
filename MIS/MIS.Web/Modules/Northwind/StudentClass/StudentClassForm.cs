﻿
namespace MIS.Northwind.Forms
{
    using Serenity.ComponentModel;
    using System;

    [FormScript("Northwind.StudentClass")]
    [BasedOnRow(typeof(Entities.StudentClassRow), CheckNames = true)]
    public class StudentClassForm
    {
        public String ClassName { get; set; }
        public String ClassNumber { get; set; }
        public String Description { get; set; }
    }
}