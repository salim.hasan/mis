﻿
namespace MIS.Northwind.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Northwind/StudentClass"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.StudentClassRow))]
    public class StudentClassController : Controller
    {
        public ActionResult Index()
        {
            return View(MVC.Views.Northwind.StudentClass.StudentClassIndex);
        }
    }
}
