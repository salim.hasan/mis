﻿
namespace MIS.Northwind.Entities
{
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), Module("Northwind"), TableName("StudentClass")]
    [DisplayName("Student Class"), InstanceName("StudentClass")]
    [ReadPermission(PermissionKeys.General)]
    [ModifyPermission(PermissionKeys.General)]
    [LookupScript]
    public sealed class StudentClassRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Class Name"), Size(15), NotNull, QuickSearch]
        public String ClassName
        {
            get { return Fields.ClassName[this]; }
            set { Fields.ClassName[this] = value; }
        }

        [DisplayName("Class Number "), QuickSearch]
        public String ClassNumber
        {
            get { return Fields.ClassNumber[this]; }
            set { Fields.ClassNumber[this] = value; }
        }

        [DisplayName("Description"), QuickSearch]
        public String Description
        {
            get { return Fields.Description[this]; }
            set { Fields.Description[this] = value; }
        }

       
        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.ClassName; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public StudentClassRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public StringField ClassName;
            public StringField ClassNumber;
            public StringField Description;
        }
    }
}