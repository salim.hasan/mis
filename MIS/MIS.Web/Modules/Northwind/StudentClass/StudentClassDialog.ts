﻿namespace MIS.Northwind {

    @Serenity.Decorators.registerClass()
    export class StudentClassDialog extends Serenity.EntityDialog<StudentClassRow, any> {
        protected getFormKey() { return StudentClassForm.formKey; }
        protected getIdProperty() { return StudentClassRow.idProperty; }
        protected getLocalTextPrefix() { return StudentClassRow.localTextPrefix; }
        protected getNameProperty() { return StudentClassRow.nameProperty; }
        protected getService() { return StudentClassService.baseUrl; }

        protected form = new StudentClassForm(this.idPrefix);
    }
}