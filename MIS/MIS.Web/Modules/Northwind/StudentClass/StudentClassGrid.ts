﻿namespace MIS.Northwind {

    @Serenity.Decorators.registerClass()
    export class StudentClassGrid extends Serenity.EntityGrid<StudentClassRow, any> {
        protected getColumnsKey() { return "Northwind.StudentClass"; }
        protected getDialogType() { return <any>StudentClassDialog; }
        protected getIdProperty() { return StudentClassRow.idProperty; }
        protected getLocalTextPrefix() { return StudentClassRow.localTextPrefix; }
        protected getService() { return StudentClassService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}