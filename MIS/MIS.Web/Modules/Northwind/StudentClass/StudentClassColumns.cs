﻿
namespace MIS.Northwind.Forms
{
    using Serenity.ComponentModel;
    using System;
    using System.ComponentModel;

    [ColumnsScript("Northwind.StudentClass")]
    [BasedOnRow(typeof(Entities.StudentClassRow), CheckNames = true)]
    public class StudentClassColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Id { get; set; }
        [EditLink]
        public String ClassName { get; set; }
        public String ClassNumber { get; set; }
        //[Width(450)]
        public String Description { get; set; }
    }
}