﻿using Serenity.ComponentModel;
using System.ComponentModel;

namespace MIS
{
    [EnumKey("Gender"), ScriptInclude]
    public enum Gender
    {
        Male = 1,
        Female = 2,
    }


    [EnumKey("Month"), ScriptInclude]
    public enum Month
    {
        January=1,
        Febuary=2,
        March=3,
    }
}