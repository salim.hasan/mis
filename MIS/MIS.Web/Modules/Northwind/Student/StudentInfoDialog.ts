﻿namespace MIS.Northwind {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    @Serenity.Decorators.maximizable()
    export class StudentInfoDialog extends Serenity.EntityDialog<StudentInfoRow, any> {
        protected getFormKey() { return StudentInfoForm.formKey; }
        protected getIdProperty() { return StudentInfoRow.idProperty; }
        protected getLocalTextPrefix() { return StudentInfoRow.localTextPrefix; }
        protected getNameProperty() { return StudentInfoRow.nameProperty; }
        protected getService() { return StudentInfoService.baseUrl; }

        protected form = new StudentInfoForm(this.idPrefix);


        constructor() {
            super();
        }

        protected afterLoadEntity() {
            super.afterLoadEntity();

            if (this.isNew()) {
                this.getNextNumber();
            }
        }

        private getNextNumber() {

            var val = Q.trimToNull(this.form.RegistrationNo.value);

            if (!val || val.length <= 1) {

                var prefix = (val || '2020').toUpperCase();

                StudentInfoService.GetNextRegistrationNo({
                    Prefix: prefix,
                    Length: prefix.length + 6
                }, response => {
                    this.form.RegistrationNo.value = response.Serial;

                    // this is to mark numerical part after prefix
                    //(this.form.AssessmentNo.element[0] as any).setSelectionRange(prefix.length, response.Serial.length);
                });
            }
        } 
    }
}