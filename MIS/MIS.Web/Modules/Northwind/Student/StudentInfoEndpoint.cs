﻿
namespace MIS.Northwind.Endpoints
{
    using Serenity.Data;
    using Serenity.Services;
    using System.Data;
    using System.Web.Mvc;
    using MyRepository = Repositories.StudentInfoRepository;
    using MyRow = Entities.StudentInfoRow;

    [RoutePrefix("Services/Northwind/StudentInfo"), Route("{action}")]
    [ConnectionKey(typeof(MyRow)), ServiceAuthorize(typeof(MyRow))]
    public class StudentInfoController : ServiceEndpoint
    {
        [HttpPost, AuthorizeCreate(typeof(MyRow))]
        public SaveResponse Create(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MyRepository().Create(uow, request);
        }

        [HttpPost, AuthorizeUpdate(typeof(MyRow))]
        public SaveResponse Update(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MyRepository().Update(uow, request);
        }

        [HttpPost, AuthorizeDelete(typeof(MyRow))]
        public DeleteResponse Delete(IUnitOfWork uow, DeleteRequest request)
        {
            return new MyRepository().Delete(uow, request);
        }

        public RetrieveResponse<MyRow> Retrieve(IDbConnection connection, RetrieveRequest request)
        {
            return new MyRepository().Retrieve(connection, request);
        }

        public ListResponse<MyRow> List(IDbConnection connection, ListRequest request)
        {
            return new MyRepository().List(connection, request);
        }

        public GetNextNumberResponse GetNextRegistrationNo(IDbConnection connection, GetNextNumberRequest request)
        {
            return GetNextNumberHelper.GetNextNumber(connection, request, MyRow.Fields.RegistrationNo);
        }

        //public ActionResult Challan(IDbConnection connection, _Ext.EntityReportRequest request)
        //{
        //    var data = Retrieve(connection, request).Entity;

        //}

        public ActionResult Challan(IDbConnection connection, RetrieveRequest request)
        {
            var data = Retrieve(connection, request).Entity;

            return View(MVC.Views.Northwind.Student.StudentInfoSingleReport, data);
        }
    }
}
