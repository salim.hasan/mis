﻿
namespace MIS.Northwind.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Northwind.StudentInfo")]
    [BasedOnRow(typeof(Entities.StudentInfoRow), CheckNames = true)]
    public class StudentInfoColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight,Hidden]
        public Int32 Id { get; set; }
        [EditLink]
        public String RegistrationNo { get; set; }
        public String StudentName { get; set; }
        public String FatherName { get; set; }
        public String ClassName { get; set; }
        [Hidden]
        public Int32 AcadamicYearId { get; set; }
        public Int32 ShiftName { get; set; }
        public Gender Gender { get; set; }
    }
}