﻿
namespace MIS.Northwind.Entities
{
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), Module("Northwind"), TableName("StudentInfo")]
    [DisplayName("StudentInfo"), InstanceName("StudentInfo")]
    [ReadPermission(PermissionKeys.General)]
    [ModifyPermission(PermissionKeys.General)]
    [LookupScript]
    public sealed class StudentInfoRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Acadamic Year"), NotNull]
        [ForeignKey(typeof(AcadamicYearRow)), LeftJoin("jAcadamicYear")]
        [LookupEditor(typeof(AcadamicYearRow)), QuickFilter]
        public Int32? AcadamicYearId
        {
            get { return Fields.AcadamicYearId[this]; }
            set { Fields.AcadamicYearId[this] = value; }
        }

        [DisplayName("Registration No."), Size(15), NotNull, QuickSearch,ReadOnly(true)]
        public String RegistrationNo
        {
            get { return Fields.RegistrationNo[this]; }
            set { Fields.RegistrationNo[this] = value; }
        }

        [DisplayName("Class Name"), NotNull]
        [ForeignKey(typeof(StudentClassRow)), LeftJoin("jStudentClass")]
        [LookupEditor(typeof(StudentClassRow)), QuickFilter]
        public Int32? ClassId
        {
            get { return Fields.ClassId[this]; }
            set { Fields.ClassId[this] = value; }
        }

        [DisplayName("Shift Name"), NotNull]
        [ForeignKey(typeof(ShiftRow)), LeftJoin("jStudentShift")]
        [LookupEditor(typeof(ShiftRow)), QuickFilter]
        public Int32? ShiftId
        {
            get { return Fields.ShiftId[this]; }
            set { Fields.ShiftId[this] = value; }
        }

        [DisplayName("Student Name"), Size(15), NotNull, QuickSearch]
        public String StudentName
        {
            get { return Fields.StudentName[this]; }
            set { Fields.StudentName[this] = value; }
        }

        [DisplayName("Father'S Name"), Size(15), NotNull, QuickSearch]
        public String FatherName
        {
            get { return Fields.FatherName[this]; }
            set { Fields.FatherName[this] = value; }
        }

        [DisplayName("Mother'S Name"), Size(15), NotNull, QuickSearch]
        public String MotherName
        {
            get { return Fields.MotherName[this]; }
            set { Fields.MotherName[this] = value; }
        }

        [DisplayName("Date Of Birth"), QuickSearch]
        public DateTime? DateOfBirth
        {
            get { return Fields.DateOfBirth[this]; }
            set { Fields.DateOfBirth[this] = value; }
        }

        [DisplayName("Gender"), QuickSearch]
        public Gender? Gender
        {
            get { return (Gender?)Fields.Gender[this]; }
            set { Fields.Gender[this] = (int?)value; }
        }


        //#region Pilotage Report Type
        //[DisplayName("Report On"), NotNull, QuickFilter, Updatable(false)]
        //public PilotageReportType? PilotageReportType { get { return (PilotageReportType?)Fields.PilotageReportType[this]; } set { Fields.PilotageReportType[this] = (int?)value; } }
        //public partial class RowFields { public Int32Field PilotageReportType; }
        //#endregion PilotageReportType


        

        [DisplayName("Remarks"), QuickSearch]
        public String Remarks
        {
            get { return Fields.Remarks[this]; }
            set { Fields.Remarks[this] = value; }
        }


        #region ForanKeys

        [DisplayName("Class Name"), Expression("jStudentClass.[ClassName]")]
        public String ClassName
        {
            get { return Fields.ClassName[this]; }
            set { Fields.ClassName[this] = value; }
        }

        [DisplayName("Shift Name"), Expression("jStudentShift.[ShiftName]")]
        public String ShiftName
        {
            get { return Fields.ShiftName[this]; }
            set { Fields.ShiftName[this] = value; }
        }

        

        #endregion ForanKeys

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }


        StringField INameRow.NameField
        {
            get { return Fields.RegistrationNo; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public StudentInfoRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public Int32Field AcadamicYearId;
            public StringField RegistrationNo;
            public Int32Field ClassId;
            public StringField ClassName;
            public Int32Field ShiftId;
            public StringField ShiftName;
            public StringField StudentName;
            public StringField FatherName;
            public StringField MotherName;
            public DateTimeField DateOfBirth;
            public Int32Field Gender;
            public StringField Remarks;
        }

       
    }
}