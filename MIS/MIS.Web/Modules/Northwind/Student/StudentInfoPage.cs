﻿
namespace MIS.Northwind.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Northwind/StudentInfo"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.StudentInfoRow))]
    public class StudentInfoController : Controller
    {
        public ActionResult Index()
        {
            return View(MVC.Views.Northwind.Student.StudentInfoIndex);
        }
    }
}
