﻿namespace MIS.Northwind{
    import fld = StudentInfoRow.Fields;

    @Serenity.Decorators.registerClass()
    export class StudentInfoGrid extends Serenity.EntityGrid<StudentInfoRow, any> {
        protected getColumnsKey() { return "Northwind.StudentInfo"; }
        protected getDialogType() { return <any>StudentInfoDialog; }
        protected getIdProperty() { return StudentInfoRow.idProperty; }
        protected getLocalTextPrefix() { return StudentInfoRow.localTextPrefix; }
        protected getService() { return StudentInfoService.baseUrl; }

        protected getDisplayName() { return 'Student Information'; }

        constructor(container: JQuery) {
            super(container);
        }


        protected getColumns() {
            let columns = super.getColumns();

            columns.splice(1, 0,
                {
                    field: 'Print',
                    name: 'Print',
                    cssClass: 'align-center',
                    width: 40,
                    minWidth: 40,
                    maxWidth: 40,
                    format: ctx => `<button title="Print" class="${true ? 'print-row2' : 'disabled'} glyphicon glyphicon-print inline-action btn btn-primary btn-xs"></button>`,
                });

            return columns;
        }

        protected onInlineActionClick(target: JQuery, recordId, item: StudentInfoRow) {
            super.onInlineActionClick(target, recordId, item);

            if (target.hasClass('print-row2')) {
                let request: _Ext.EntityReportRequest = { EntityId: recordId };
                request.ReportDesignPath = "~/Modules/Northwind/Student/StudentInfoSingleReport.cshtml";

                Q.postToService({ service: Q.resolveUrl(this.getService() + '/Challan'), request: request, target: '_blank' });


            }

        }
    }
}