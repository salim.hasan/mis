﻿
namespace MIS.Northwind.Forms
{
    using Serenity.ComponentModel;
    using System;

    [FormScript("Northwind.StudentInfo")]
    [BasedOnRow(typeof(Entities.StudentInfoRow), CheckNames = true)]
    public class StudentInfoForm
    {
        
        [HalfWidth(UntilNext =true)]
        public Int32 AcadamicYearId { get; set; }
        public String RegistrationNo { get; set; }
        public Int32 ClassId { get; set; }
        public Int32 ShiftId { get; set; }
        [FullWidth]
        public String StudentName { get; set; }
        [HalfWidth(UntilNext =true)]
        public String FatherName { get; set; }
        public String MotherName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public Int32 Gender { get; set; }

        [FullWidth]
        [TextAreaEditor(Rows = 3)]
        public String Remarks { get; set; }
    }
}